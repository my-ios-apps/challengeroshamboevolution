//
//  HistoryViewController.swift
//  RockPaperScissors
//
//  Created by Mario Cezzare on 12/12/18.
//  Copyright © 2018 Gabrielle Miller-Messner. All rights reserved.
//

import Foundation
import UIKit

class HistoryViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Historic List
    var history = [RPSMatch]()
    
    // MARK : Outlet
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Table View Data Source Methods
    
    // number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
    
    
    // cell for row at index path
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // TODO: Implement method
        // 1. Dequeue a reusable cell from the table, using the correct “reuse identifier”
        let cellId = "HistoryCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId , for: indexPath)
        
        // 2. Find the model object that corresponds to that row
        let historyRow = self.history[(indexPath as NSIndexPath).row]
        
        // 3. Set the images and labels in the cell with the data from the model object
        let resultText = victoryStatusDescription(historyRow)
        cell.textLabel?.text = resultText
        cell.detailTextLabel!.text = "\(historyRow.p1) vs. \(historyRow.p2)"
        
//        let image :UIImage = ResultViewController.imageForMatch(historyRow )
        let image :UIImage = imageForMatch(historyRow)
        cell.imageView?.image = image
        
        // decorate cells
        switch resultText {
        case "Tie.":
            cell.backgroundColor = UIColor.lightGray
        case "Win!":
            cell.backgroundColor = UIColor.green
        case "Loss.":
            cell.backgroundColor = UIColor.red
            
        default:
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    
    // MARK: Duplicate Code, TODO: Remove
    func imageForMatch(_ match: RPSMatch) -> UIImage {
        
        var name = ""
        
        switch (match.winner) {
        case .rock:
            name = "RockCrushesScissors"
        case .paper:
            name = "PaperCoversRock"
        case .scissors:
            name = "ScissorsCutPaper"
        }
        
        if match.p1 == match.p2 {
            name = "itsATie"
        }
        return UIImage(named: name)!
    }
    
    // MARK: String for present result history
    func victoryStatusDescription(_ match: RPSMatch) -> String {
        
        if (match.p1 == match.p2) {
            return "Tie."
        } else if (match.p1.defeats(match.p2)) {
            return "Win!"
        } else {
            return "Loss."
        }
    }
    
    // MARK: Actions
    
    @IBAction func dismissHistory(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
